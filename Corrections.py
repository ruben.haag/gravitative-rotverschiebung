from __future__ import division
import numpy as np
from PIL import Image
from PIL.TiffTags import TAGS


# Path of the Picture: [date_time_type.tiff]
# TODO über Zweitliche Entwicklung ca. 10 min. Mitteln
# TODO NUmpy Integer Arrays verwenden
class Picture():
	# Class Attribute
	# Initializer / Instance Attributes
	
	def __init__(self, path):
		self.path = {'norm': path}
		self.array2D = self.getImg()
		self.data = self.imageData()
		
		self.height, self.length = self.array2D.shape
		
	def show(self):
		with Image.open(self.path['norm']) as img:
			img.show()


	def getImg(self):
		'''Transforms a monochrome TIFF Picture into a 2D Array:
		Parameters:
		path (string): path of the Picture

		Returns:
		2D NmPy int Array: the Picture as monochrome 2D Array
		'''
		with Image.open(self.path['norm']).convert('L') as img:
			array2D = np.array(img)
		#print(array2D[1][2])

		#TODO richtig drehen und evtl. Spiegeln
		#TODO Implementieren
		#print(array2D.shape)
		return array2D

	def imageData(self):
		''' Reads the Image Data (date, time, etc.) and returns it as a String
			Parameters:
			path (string): path of the Picture

			Returns:
			string: Image data
		'''
		with Image.open(self.path['norm']) as img:
			data = {TAGS[key]: img.tag[key] for key in img.tag.keys()}
		#TODO Implementieren
		return data

	def darkfield(self, darkfieldPath):
		''' Darkfield corrections
			Parameters:
				image(2D NmPy float Array) : Picture
				darkfieldPath: Path to Darkfield Image

			Returns
			2D NumPy int Array: image
		'''

		self.path['dark'] = darkfieldPath
		with Image.open(self.path['dark']).convert('L') as img:
			field = np.array(img)
		self.array2D -= field
		return self.array2D


	def flatfield(self, picture, flatfieldPath):
		''' flatfield corrections
			Parameters:
				image(2D NmPy float Array) : Picture
				flatfield : flatfield Image

			Returns
			2D NumPy int Array: corrected image
		'''
		self.path['flat'] = flatfieldPath

		with Image.open(self.path['flat']).convert('L') as img:
			field = np.array(img)
		avgFlat = np.average(field)
		self.array2D = self.array2D * field * avgFlat
		return self.array2D

	def to1DSpectrum(self, isSpectrum=False):
		'''
		Parameter:
			image(2D NmPy int Array) : Picture[lines][rows]
			isSpectrum (boolean): True if the image does only consist of a spectrum. Fals if not.
		Returns:
			[int]
		'''
		avgLine = 0
		for line in self.array2D:
			avgLine += (np.average(line))
		avgLine/= self.height
		

		# TODO minTotal festlegen
		def isIlluminated(self, line, avgLine):
			''' Returns True, if the line has above average brightness otherwise false'''
			total = 0
					
			if np.average(line) < avgLine:
				return False
			return True
		test = self.array2D
		
		spectrum = np.zeros(self.length)
		if isSpectrum:
			for line in self.array2D:
				spectrum+=line
			return spectrum

		for line in self.array2D:
			if isIlluminated(self, line, avgLine):
				spectrum += line
		
		return spectrum


	def cosmetics(self, darkfieldPath='', flatfieldPath=''):
		'''Takes the picture and transforms into a 1D Spektrum with optional dark and flatfield corrections
			Parameters:
			path(string): /.../number 

			Returns:
				[int]: Spektrum
		'''
		if len(darkfieldPath) > 1:
			self.darkfield(self, darkfieldPath)
		if len(flatfieldPath) > 1:
			self.flatfield(self, flatfieldPath)
