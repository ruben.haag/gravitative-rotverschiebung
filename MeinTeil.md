# Vortrag 1

## Was Muss rein?

### Aufbau
- 3D Modell ohne Breadboard
  - Nacheinander Einblenden? Dann kann das schön aufgeteilt werden
  - 3D drucke sind Cool -> sollten auf jeden Fall erwähnt werden
  - ohne Breadboard mit Weißem Hintergrund
  - Beugungsbild eines Gitters?
  - Fokus?

### Messmethode
- Was ist ein Spektrum?
- Absorbtion
- Frauenhofer Linien
- Tellurische Linien
- Was muss noch rausgerechnet werden?
- Keine sonnenflecken :(


### Materie
  - kurze Einführung in die allgemeine Relativitätstheorie.
  - Gravitative Rotverschiebung


# Vortrag 2

# Schriftliche Ausarbeitung
## Kamera Software und einrichtung des PI
- Hintergrundmethoden
  - Gain is a Pain in the Ass
  - Diverse optimierungen
- Interface und Fernzugriff
  - Plotting Interface
  - Fernzugriff wäre theoretisch für alle Physik Studierende möglich

## Fokussierung mit dem Monomode Laser
### Was ist Spektraler und Räumlicher Fokus
### Freiheitsgrade

## Optisches System einmal durchrechnen
- Näherungen
  - Irgendwie an meine Rechnungen von damals kommen?
- Raytraycing
  - [ ] HP fragen, wie er das gemacht hat

## Auswertung
### Flat Field und Dark Field korrektur
### Strucktur des Auswertungsprogramms

