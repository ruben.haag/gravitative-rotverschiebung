class Fitter():
    """
    Manages line fitting in spectral data. Stores fitting tasks / paramters to apply them in the same way on every data set provided through ``.fit``. Collects the results from these in line core pixel coordinates separed in solar and athmospheric.  
    Takes
    -----
    \a pxl_errs >>> numpy.array(float*)  

    permanent error estimate in value for each pixel in data got through ``.fit``. Must have same length as data.
    \a ref_keys >>> list(str*)

    line identification keys for lines to be found in later data to calculate wavelength solution from - eg athmospherics. See Atlas(.py) for choices.
    \a sun_keys >>> list(str*)
    
    same for the shift-relevant ones - eg solars.
    \a gauss_width >>> float  = 0.003
    
    radius of data arround line core to be used for fitting a gauss profile to - eg for the solars. In nanometers (<- same as units in Atlas(.py)). 
    \a voigt_width >>> float < 1  = 0.9
    
    same for voigt profile - eg for the athmospherics. In relative intensity of line core (90% of difference of neighbouring maxima and line core intensity)
    \a peak_order >>> int  = 10 
    
    number of pixels of minimal difference between neighbouring extrema. Elemental in line core (and background) recognition.
    """
    
    def fit(self, *args, **kwargs):
        """
        Perform spectral lines identification and fit on data. Stores ref and sun lines pixel coordinates.
        Takes
        -----
        \a data >>> np.array(float*)

        the one dimensional spectral data as given by the pipeline. gets attribute
        \a show >>> bool  = self.show (= 1stly True, then False)

        parameter if fit on this data set should be visualized (using Intendant class)
        """
        return self.__fit(*args, **kwargs)

    def findCores(self):
        """
        Find line cores (peaks) as local minima in data using scipy.signal.argrelmin with order = self.peak_order. Select most haeviest ones.
        Returns
        -------
        peaks >>> list(int*)

        point of maximum intensity leak for as much lines as to find
        """
    def fitAthmos(self, peaks):
        """
        Athmospheric line specific wrap to fit voigt profile with .fitLine using data slice given by .getBordersVoigt.
        Takes
        -----
        \a peaks >>> list(int*)
        
        Locations of line centers in data to treat as athmospherics
        Returns
        -------
        \a athmos >>> list(tuple(float*2))        

        athmospheric line core pixel coordinates from fit
        """
    def fitSolars(self, peaks): 
        """
        Solar line specific wrap to fit gauss profile with .fitLine using data slice given by .getBordersGauss.
        Takes
        -----
        \a peaks >>> list(int*)
        
        Locations of line centers in data to treat as solars
        Returns
        -------
        \a solars >>> list(tupel(float*2))        

        solar line core wavelengths with error from fitted pixel coordinate and wavelsolution from athmos with Calibrator class 
        """
    def fitLine(self, profile, slice, color=None): #slice(0,-1)
        """
        Fit profile on slice from data and return the fitted line core pixel coordinate with error. \n
        Assumes the line cores coordinate in data is parameter ``0`` in profile.
        Takes
        -----
        \a profile >>> callable

        Method to perfom fit with scipy.optimize.curve_fit. Directly feeded into
        \a slice >>> slice

        slice to take from data to fit profile on to
        \a color >>> str  = None

        color for this line to be plotted in. if False or nothing it will not be plotted, as well if no intendant is assigned to self
        Returns
        -------
        \a lines >>> list(tupel(float*2))        

        line core pixel coordinates 
        """

    def calc(self, pxl):
        """
        Calculate wavelengths out of pixels with polynomial wavelength calibration.
        Parameters
        ----------
        \a pxls >>> list(tuple(float*2)) 
        | pixel coordinates of lines with error
        
        Returns:
        -----
        wavels  (list(tupel(floats))):  wavelengths with error
        """