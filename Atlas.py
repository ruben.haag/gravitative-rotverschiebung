'''
Global dictionary containig spectral lines. To be used as reference for wavelength solution or shift calculation. Floats in nanometers.
'''
#TODO Wellenlängen genauer herausfinden mit Fehlern!!!
Spec_Atlas = {'a':      (630.167    , 0.0004), 
              'atm_a':  (630.2      , 0.001),
              'b':      (630.249    , 0.001),
              'atm_b':  (630.275    , 0.001)}